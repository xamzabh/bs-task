export interface IAppState {
  allEntities: IEntity[];
  openedParents: Dictionary<boolean>;
}

export interface IEntity {
  id: number;
  date: number;
  parent: number | null;
}

export interface Dictionary<TValue> {
  [index: string]: TValue | undefined;
  [index: number]: TValue | undefined;
}


