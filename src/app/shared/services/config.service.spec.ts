import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ConfigService} from './config.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Store, StoreModule} from '@ngrx/store';
import {dataReducer} from '../store/config';
import {AppComponent} from '../../app.component';
import {DebugElement} from '@angular/core';

describe('ConfigService', () => {
  let service: ConfigService;

  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(dataReducer)
      ],
      providers: [ConfigService]
    });
    service = TestBed.inject(ConfigService);
  });

  beforeEach(() => {
    service = TestBed.inject(ConfigService);
    store = TestBed.inject(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
