import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IEntity} from '../types';
import {Store} from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(protected http: HttpClient,
              protected store: Store) {
  }

  public loadConfig(): Promise<IEntity[]> {
    return this.http.get<IEntity[]>('./assets/mocked-data.json').toPromise();
  }

  public generateRandomJson(elementsCount: number = 50): IEntity[] {
    const arr = [];
    for (let i = 0; i < elementsCount; i++) {
      arr.push({
        id: i + 1,
        date: Date.now() - (Math.floor(Math.random() * (Date.now() - new Date(0).getTime()))),
        parent: i < 5 ? null : arr[Math.floor(Math.random() * arr.length)].id,
      });
    }
    return arr;
  }

}
