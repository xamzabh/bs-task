import {Dictionary, IAppState, IEntity} from '../../types';
import {createSelector} from '@ngrx/store';


export const selectAllEntities = createSelector(
  value => value,
  (state: { data: IAppState }): IEntity[] => state.data.allEntities
);
export const selectOpenedParents = createSelector(
  value => value,
  (state: { data: IAppState }): Dictionary<boolean> => state.data.openedParents
);

export const selectRootParents = createSelector(
  selectAllEntities,
  (state: IEntity[]) => state.filter(entity => !entity.parent)
);

export const selectEntity = createSelector(
  selectAllEntities,
  (state: IEntity[], props: { id: number }) => state.find(entity => entity.id === props.id)
);

export const selectChildren = createSelector(
  selectAllEntities,
  (state: IEntity[], props: { id: number }) => state.filter(entity => entity.parent === props.id)
);

export const selectIsParentOpen = createSelector(
  selectOpenedParents,
  (state: Dictionary<boolean>, props: { id: number }) => state[props.id] !== undefined
);
