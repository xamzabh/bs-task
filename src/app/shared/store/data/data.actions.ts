import {createAction, props} from '@ngrx/store';
import {IEntity} from '../../types';

export enum EDataActions {
  InitEntities = 'Add initial entities in collection',
  OpenParent = 'Open parent view',
  CloseParent = 'Close parent view',
}

export const initEntities = createAction(EDataActions.InitEntities, props<{ entities: IEntity[] }>());
export const openParent = createAction(EDataActions.OpenParent, props<{ id: number }>());
export const closeParent = createAction(EDataActions.CloseParent, props<{ id: number }>());
