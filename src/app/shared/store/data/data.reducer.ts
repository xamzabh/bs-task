import {IAppState} from '../../types';
import {createReducer, on} from '@ngrx/store';
import {initEntities, closeParent, openParent, EDataActions} from './data.actions';
import {TypedAction} from '@ngrx/store/src/models';

export const initialState: IAppState = {
  allEntities: [],
  openedParents: {}
};

const reducer = createReducer(
  initialState,
  on(initEntities, (state, data) => {
    return {
      allEntities: data.entities,
      openedParents: state.openedParents,
    };
  }),
  on(openParent, (state, data) => {
    const openedParents = {...state.openedParents};

    openedParents[data.id] = true;

    return {
      allEntities: state.allEntities,
      openedParents: openedParents,
    };
  }),
  on(closeParent, (state, data) => {
    const openedParents = {...state.openedParents};
    delete openedParents[data.id];

    closeChildrenInDepth(data.id);

    function closeChildrenInDepth(parentId: number) {
      const openedChildren = state.allEntities.filter(entity => entity.parent === parentId && openedParents[entity.id]);
      openedChildren.forEach(child => {
        delete openedParents[child.id];
        closeChildrenInDepth(child.id);
      });
    }

    return {
      allEntities: state.allEntities,
      openedParents: openedParents,
    };
  }),
);


export function allEntitiesReducer(state: IAppState, action: TypedAction<EDataActions>) {
  return reducer(state, action);
}
