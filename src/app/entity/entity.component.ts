import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {IEntity} from '../shared/types';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {selectChildren, selectIsParentOpen} from '../shared/store/data/data.selectors';
import {closeParent, openParent} from '../shared/store/data/data.actions';

@Component({
  selector: 'app-entity',
  templateUrl: './entity.component.html',
  styleUrls: ['./entity.component.scss']
})
export class EntityComponent implements OnChanges, OnDestroy {
  @Input() public entity: IEntity;

  public children: IEntity[];
  public shouldShowChildren: boolean;

  protected childrenSubscription: Subscription;
  protected isParentOpenSubscription: Subscription;

  constructor(protected store: Store,
              protected cdr: ChangeDetectorRef) {
    this.children = [];
    // this.cdr.detach();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cdr.detectChanges();
    if (changes.entity && changes.entity.isFirstChange()) {
      this.childrenSubscription = this.store.select(selectChildren, this.entity)
        .subscribe(data => {
          this.children = data;
          this.cdr.detectChanges();
        });
      this.isParentOpenSubscription = this.store.select(selectIsParentOpen, this.entity)
        .subscribe(data => {
          this.shouldShowChildren = data;
          this.cdr.detectChanges();
        });
    }
  }

  ngOnDestroy(): void {
    if (this.isParentOpenSubscription) {
      this.isParentOpenSubscription.unsubscribe();
    }
    if (this.childrenSubscription) {
      this.childrenSubscription.unsubscribe();
    }
  }

  async onParentClick(): Promise<void> {
    if (this.shouldShowChildren) {
      this.store.dispatch(closeParent(this.entity));
    } else {
      this.store.dispatch(openParent(this.entity));
    }
    this.cdr.detectChanges();
  }


}
