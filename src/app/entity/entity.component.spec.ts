import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EntityComponent} from './entity.component';
import {Store, StoreModule} from '@ngrx/store';
import {dataReducer} from '../shared/store/config';
import {DebugElement, SimpleChange} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('EntityComponent', () => {
  let component: EntityComponent;
  let fixture: ComponentFixture<EntityComponent>;
  let de: DebugElement;
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EntityComponent],
      imports: [
        StoreModule.forRoot(dataReducer)
      ],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    store = TestBed.inject(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show id', () => {
    component.entity = {id: 5, date: new Date().getTime(), parent: null};
    component.ngOnChanges({
      entity: new SimpleChange(undefined, component.entity, true)
    });
    const idSpan = de.query(By.css('.parent-container .entity-info .entity-id'));
    expect(idSpan.nativeElement.innerHTML).toEqual(component.entity.id + '.');
  });

  it('should have and show 1 child', () => {
    component.entity = {id: 2, date: new Date().getTime(), parent: null};
    component.children = [
      {id: 11, date: 286093415685, parent: 2}
    ];
    component.shouldShowChildren = true;
    component.ngOnChanges({
      entity: new SimpleChange(undefined, component.entity, false)
    });
    const childrenDiv = de.query(By.css('.children-container'));
    expect(childrenDiv.children.length).toEqual(1);
  });

  it('should show no children', () => {
    component.entity = {id: 2, date: new Date().getTime(), parent: null};
    component.children = [];
    fixture.detectChanges();
    component.ngOnChanges({
      entity: new SimpleChange(undefined, component.entity, false)
    });
    const idSpan = de.query(By.css('.parent-container .entity-info .entity-id'));
    expect(idSpan.nativeElement.innerHTML).toEqual(component.entity.id + '.');
  });
});


