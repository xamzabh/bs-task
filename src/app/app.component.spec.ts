import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {ConfigService} from './shared/services/config.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DebugElement} from '@angular/core';
import {Store, StoreModule} from '@ngrx/store';
import {dataReducer} from './shared/store/config';
import {EntityComponent} from './entity/entity.component';
import {By} from '@angular/platform-browser';
import {initEntities} from './shared/store/data/data.actions';
import {IEntity} from './shared/types';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let de: DebugElement;

  let configService: ConfigService;
  let store: Store;

  let loadConfigSpy: jasmine.Spy;
  let getRootParentsSpy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        EntityComponent,
      ],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(dataReducer)
      ],
      providers: []
    }).compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    configService = TestBed.inject(ConfigService);
    store = TestBed.inject(Store);

    const fakeLoadConfig: Promise<IEntity[]> = new Promise<IEntity[]>(resolve => {
      const data = configService.generateRandomJson();
      store.dispatch(initEntities({entities: data}));
      return resolve(data);
    });

    loadConfigSpy = spyOn(configService, 'loadConfig').and.returnValue(fakeLoadConfig);
    getRootParentsSpy = spyOn(component, 'getRootParents').and.callThrough();
  });


  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should load config', (done) => {
    component.ngOnInit();
    expect(loadConfigSpy).toHaveBeenCalled();
    loadConfigSpy.calls.mostRecent().returnValue.then(data => {
      expect(data.length).toBeGreaterThan(0);
      done();
    });
  });

  it('should get only rootParents', (done) => {
    component.ngOnInit();
    loadConfigSpy.calls.mostRecent().returnValue.then(() => {
      getRootParentsSpy.calls.mostRecent().returnValue.subscribe(() => {
        expect(component.rootParents.length).toBeGreaterThan(0);
        expect(component.rootParents.filter(el => el.parent).length).toEqual(0);
        done();
      });
    });
  });

  it('should show parents elements', (done) => {
    component.ngOnInit();
    loadConfigSpy.calls.mostRecent().returnValue.then(() => {
      getRootParentsSpy.calls.mostRecent().returnValue.subscribe(() => {
        expect(de.queryAll(By.css('app-entity')).length).toEqual(component.rootParents.length);
        done();
      });
    });
  });
});


