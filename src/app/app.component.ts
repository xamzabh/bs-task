import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable, Subscription} from 'rxjs';
import {IEntity} from './shared/types';
import {ConfigService} from './shared/services/config.service';
import {selectRootParents} from './shared/store/data/data.selectors';
import {initEntities} from './shared/store/data/data.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public rootParents: IEntity[];

  protected rootParentsSubscription: Subscription;

  constructor(protected store: Store,
              protected configService: ConfigService,
              protected cdr: ChangeDetectorRef) {

    this.rootParents = [];

    this.cdr.detach();
  }


  ngOnInit(): void {
    this.configService.loadConfig()
      .then(data => this.store.dispatch(initEntities({entities: data})));
    this.rootParentsSubscription = this.getRootParents()
      .subscribe(data => {
        this.rootParents = data;
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy(): void {
    if (this.rootParentsSubscription) {
      this.rootParentsSubscription.unsubscribe();
    }
  }

  public getRootParents(): Observable<IEntity[]> {
    return this.store.select(selectRootParents);
  }
}
