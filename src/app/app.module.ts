import {BrowserModule} from '@angular/platform-browser';
import {NgModule, OnInit} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';

import {AppComponent} from './app.component';
import {EntityComponent} from './entity/entity.component';
import {dataReducer} from './shared/store/config';


@NgModule({
  declarations: [
    AppComponent,
    EntityComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot(dataReducer)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule{
  constructor() {
  }
}
