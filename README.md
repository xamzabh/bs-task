# Task
Please create a single page application using Angular 4+, which should be a parent/child view model for given data.

Please create 2 separate components - one for parent and one for child view. When you click on a parent, the child information should be displayed in the child view.

Use of redux pattern (NgRx Store) and reactive programming is highly recommended, as well as unit testing.

The data will be in the format: [{"id" : 1, “date”: 1336333636, “parent" : null}, {"id" : 2, “date”: 8487474477, "parent" : 1}]

Please mock the data in a separate json file.

The result should be a ready to start application with all necessary instructions to build and deploy.

# BsTask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
